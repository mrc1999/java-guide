## JAVA 8 新特性实用总结

作为一个工作两年多的 `老` 程序猿，虽然一开始就使用 `jdk1.8` 作为学习和使用的版本，随着技术的迭代，现有的 `JDK` 版本从两年前到现在，已经飞速发展到了 `JDK 15` 。真的感觉有点学不动了，更新速度太快了，不过相比于现有系统以及国内趋势。大多公司还是采用最基础的 `1.8` 作为线上环境来使用。也是没有任何问题的，不过我们真的 `会使用` JAVA8 吗？ 

> https://www.oracle.com/java/technologies/java-se-glance.html



### 新特性概述

本小结主要从 `Lambda` 表达式入手，由浅入深，按照实用性作为排行，逐步讲解新特性带给开发人员的快乐，如何更好的简化代码，优化可读性。这才是我们学习总结这一小节的一个目的。



### 你会使用遍历循环？

从最基础的循环开始，循环无非是我们刚学习的时候就需要接触 `for` 这个最基本的循环结构，而且在后面的工作总都会大量使用的一个结构，如何更好的简化它呢？

```java
// 建立测试集合
List<Integer> list = Arrays.asList(1, 2, 2, 3, 4, 5, 5, 6);

// 基础循环
System.out.println("----------------------------1 基础循环");
for (int i = 0; i < list.size(); i++) {
    System.out.println(list.get(i));
}

// 语法糖方式
System.out.println("----------------------------2 迭代器语法糖");
for (Integer i : list) {
    System.out.println(i);
}

// lambda 表达式简写
System.out.println("----------------------------3 lambda");
list.forEach(item -> System.out.println(item));

// 使用lambda 方法引用
System.out.println("----------------------------4 lambda");
list.forEach(System.out::println);
```

从上面的代码我们可以看出，随着 `lambda` 方式的引入，代码变得越来越简化，而且更加容易读懂，写的东西也越来越少，

1. 第一种方式则是我们常规的操作方式，一般适用于需要 `下标` 逻辑的业务中。

2. 第二种则是迭代器语法糖，对于开发者而言写起来便捷，不过对于代码的编译而言，编译后的代码任是迭代器的方式，只不过语法简单了。

   ```java
   // 以下为编译后的代码
   Iterator var4 = list.iterator();
   
   while(var4.hasNext()) {
       Integer i = (Integer)var4.next();
       System.out.println(i);
   }
   ```

3. lambda 则是一种函数式的表达方式，item 作为我们循环的参数，而箭头后则是我们需要执行的代码块，一句代码完全不必使用 `{}` 

4. lambda `方法引用` 则是一种全新的方式，`引用` 二字经常被我们使用，一般在对象的引用处有表达的含义，简而言之就是 `一个值可以从一个地方引用过来使用` ，但是现在，方法完全可以被看做一个 `值` 一样，也可以随意拿过来使用~



#### forEach

可能朋友们就会有疑惑，为什么 `forEach` 的地方就可以使用 `lambda` 表达式呢，其他地方怎么不行？我们来看看源码

```java
default void forEach(Consumer<? super T> action) {
    Objects.requireNonNull(action);
    for (T t : this) {
        action.accept(t);
    }
}
```

我们发现 `Consumer` 是一个接口，内部仍然使用 `for语法糖` 形式来执行集合，调用了 `accept` 方法。



#### Consumer

> 消费者接口，适用于入参处理，无返回值

```java
@FunctionalInterface
public interface Consumer<T> {
    void accept(T t);
```

发现这个接口和其他接口唯一的不同点就是 `@FunctionalInterface` 

其实这个注解就是来告诉编译器，这个接口下的 `accept` 方法可以使用函数式写法来描述。有了这个注解的定义，我们就可以愉快的使用函数式lambda 表达式了。

`消费者接口` 作为JDK 自带的函数式接口，所处于 `java.util.function` 包下，并且支持链式操作，

接受一个指定的泛型，内部处理后，无返回值

```java
// 无返回的处理
Consumer<String> custom = (str) -> System.out.println("first" + str);
Consumer<String> desc = custom.andThen((str) -> System.out.println("second" + str));

desc.accept("hello");
--------------------------
firsthello
secondhello   
```

> 稍稍总结一下lambda 的基础语法：
>
> （参数）-> 一行执行代码
>
> （参数）-> {多行执行代码}

**单个参数完全可以省略参数的括号。**



#### default 

> 默认实现，子类无需重写接口定义的关键词

上面的Consumer使用中，我们发现，有一个默认实现的接口，顺便来说明一下

```java
default Consumer<T> andThen(Consumer<? super T> after) {
    Objects.requireNonNull(after);
    return (T t) -> { accept(t); after.accept(t); };
}
```

`default` 提供默认的实现方式，实现类无需重写这个方法的定义，而可以直接使用。



#### 方法引用

> 把方法也可以作为值一样来引用使用。

```java
// 使用lambda 方法引用
System.out.println("----------------------------4 lambda");
list.forEach(System.out::println);
```

博主这里的理解是：引用的方法需要与定义处： `default void forEach(Consumer<? super T> action)` 

所需要的lambda 表达式具有相同的入参个数与返回类型，才可以引用。 

例如：`Consumer` 接口接受的lambda 形式为：`item -> System.out.println(item)`

而我们引用的`System.out::println` 刚好具备这样的形式。

```java
public void println(Object x) {
    String s = String.valueOf(x);
    synchronized (this) {
        print(s);
        newLine();
    }
}
```



### 优雅判空

我们都知道，JAVA 里面最讨厌的一个异常就是`NPE=NullPointerException` 空指针异常，为了避免空指针异常，我们经常不少使用`if` 作为判断，这样的判断多了就容易让人看着恼火。例如如下代码：

```java
Person person = new Person("test", 1);
if (person != null) {
    if (person.getName() != null) {
        System.out.println("123" + person.getName());
    } else {
		// do something
    }
} else {
	// do something
}
```

假设我们有一个`person` 对象，首先判断它是否为空，如果不为空，则取值，而后再获取 `name` 成员变量，不为空则拼接打印。这样两层判断的逻辑在代码里经常会见到，学习了 `Optional` 以后，我们的以上逻辑就可以修改为如下：

```java
// 最佳实践
Optional.ofNullable(person).map(p -> p.getName()).map(string -> string.concat("123")).ifPresent(System.out::println);
```



#### Function

> 入参并返回一个指定类型，可以理解为转换。

首先发现 `map` 接受一个 `Function<? super T, ? extends U> mapper` ,具体如何使用Function

```java
@FunctionalInterface
public interface Function<T, R> {
    R apply(T t);
```

```java
// 链式转换
Function<String,Integer> stringToInteger = Integer::valueOf;
// andThen 将前一个处理的返回值作为后一个处理的入参
Function<String,String> integerToString = stringToInteger.andThen(Integer::toHexString);

String hex = integerToString.apply("123");
System.out.println(hex);// 7b
```



#### Optional

> 优雅判断空，并且执行对应操作

`Optional` 对于 `NPE` 有着很好的解决方式，可以解决我们多重if 的优化，不仅美观，而且非常优雅。

```java
// 如果person 为null 则触发异常
Optional.of(person);
// 如果person1 为 null 则返回empty
Optional.ofNullable(person1);
```

以上是创建实例的两种方式，一般常用第二种，第一种如果有 `null` 的情况则会触发 `NPE` 到头来还是没有处理掉这个异常，所以不建议使用。

```java
private Optional() {
    this.value = null;
}
```

```
isPresent(): 如果不为空则返回true。
get(): 获取当前包含的值，若是value=null 则抛出NPE
orElse(T other): 如果当前实例包含值为null,则返回other;
ifPresent(Consumer<? super T> consumer): 若当前实例不为空，则执行这个消费者consumer,否则返回EMPTY
```



### Stream

`stream` 作为 JAVA8 最核心的内容，融汇贯通的掌握其精髓，对开发者而言，无非是一把打开新世界大门的钥匙。从宏观的角度来讲，一个语言处理最多的就是数据的集合，比如 `List<?>`  



#### filter

> 过滤器，过滤出你想要的集合元素。

```java
List<Integer> list = Arrays.asList(1, 2, 3, 3, 4, 5, 5, 6);
// 筛选偶数
long num = list.stream().filter(item -> item % 2 == 0).count(); // 3
```

这里通过简单的筛选，筛选的条件是偶数，并且最终统计它的个数。

这里的 `filter` 接受一个 `filter(Predicate<? super T> predicate)`

> count 简而言之了，就是统计前方表达式所产生的新集合个数。



#### Predicate

> 断言，也是一个函数式接口，可以使用lambda 表达式。

```java
@FunctionalInterface
public interface Predicate<T> {

    boolean test(T t);
```

`Predicate` 主要实现其 `test` 接口，通过逻辑执行，返回一个 `boolean` 来判断当前元素是否可用。

```java
// 断言字符串长度大于0
Predicate<String> stringEmpty = (str) -> str.length() > 0;
Predicate<String> startHello = (str) -> str.startsWith("hello");

System.out.println("test 空字符=" + stringEmpty.test(""));
System.out.println("test hello=" + stringEmpty.test("hello"));

// and 合并两个检验接口，同时满足即可 or 只要有一个满足即可
System.out.println("test and hello world=" + stringEmpty.and(startHello).test("hello world"));
System.out.println("test or world=" + stringEmpty.or(startHello).test("world"));
----------------------
test 空字符=false
test hello=true
test and hello world=true
test or world=true
```



#### map

> map 可以理解为映射，处理每个元素，并且返回任何类型。支持链式map,
>
> 上层map的返回值作为下层map的参数值。

```java
List<Person> people = Arrays.asList(new Person("hello", 1), new Person("world", 2));
// 将每一个元素的name 组装成一个新的集合。
List<String> names = people.stream().map(item -> item.getName()).collect(Collectors.toList());
System.out.println(names);

// 多重map处理
List<String> concat = people.stream().map(item -> item.getName()).map(name -> name.concat("-concat")).collect(Collectors.toList());
System.out.println(concat);
-------------------
[hello, world]
[hello-concat, world-concat]
```

map 接受一个 `map(Function<? super T, ? extends R> mapper)` 我们上面已经讨论过这个了。



#### sorted

> 对元素进行排序，可以使用默认，也可以自定义排序规则。

```java
List<String> sortedList = Arrays.asList("acc", "dee", "zdd", "wee", "abb", "ccd");

// 默认排序，字典顺序，第一个字母相同，则比较第二个
List<String> sorted = sortedList.stream().sorted().collect(Collectors.toList());
System.out.println(sorted);

// 自定义实现，只比较第一个字符
List<String> sorted2 = sortedList.stream().sorted((str1, str2) -> str1.charAt(1) - str2.charAt(1)).collect(Collectors.toList());
System.out.println(sorted2);
---------------------------
[abb, acc, ccd, dee, wee, zdd]
// 可以发现自定义的排序没有比较第二个字母
[acc, abb, ccd, dee, wee, zdd]   
```

我们发现 `sorted` 接受一个 `Comparator<? super T> comparator` 



#### Comparator

> 比较器，也是函数式接口，不必多说，自然可以使用lambda

```java
@FunctionalInterface
public interface Comparator<T> {

    int compare(T o1, T o2);
```

```java
Comparator<String> comparator = (str1, str2) -> str1.charAt(0) - str2.charAt(0);

// 自定义比较第一位字母
int a = comparator.compare("abb", "acc");
System.out.println(a);

// 再次比较，如果第一个返回0，则直接返回结果，否则进行二次比较
int b = comparator.thenComparing((str1, str2) -> str1.charAt(1) - str2.charAt(1)).compare("abb", "acc");
System.out.println(b);

------------------------------
0
-1
```

比较器返回一个int 值，这个int 则表示两个元素的排列顺序，按照 [ASCII表](https://tool.oschina.net/commons?type=4) 指示的值大小，如果两个元素的差值`a-b>0` 则 a在前，b在后



#### allMatch/anyMatch

> 同样，Match 用来处理当前序列中，全部满足、或者部分满足，返回一个布尔值

```java
List<String> sortedList = Arrays.asList("acc", "dee", "zdd", "wee", "abb", "ccd");

// 所有的元素都断言通过，就返回true,否则false
boolean startWithA = sortedList.stream().allMatch(str -> str.startsWith("a"));
System.out.println(startWithA);

// 只要有一个满足就返回true
boolean hasA = sortedList.stream().anyMatch(str -> str.startsWith("a"));
System.out.println(hasA);
------------------------
false
true
```

以上就是 `stream` 常用的一些总结，总结了一些非常常用的，未总结到的内容下期补充。

### 其他

这里提一下局部变量final 语义。

#### 自定义函数式接口

模仿以上的任意一个函数接口，我们可以写出这样的一个转换接口，将指定类型转换为指定类型

```java
@FunctionalInterface
public interface FunctionInterface<A, R> {

    R cover(A t);
}
```

通过自定义函数接口，我们可以写出如下代码，来进行转换，不过涉及到一些参数的改变。

```java
// num 局部变量如果在lambda 中使用，则隐式含有final 语义
final int num = 1;
FunctionInterface<String, Integer> function4 = (val) -> Integer.valueOf(val + num);
Integer result4 = function4.cover("12");
// num = 2; // 这里不能改变，修改则不能通过编译
```

**以上内容均为博主自我学习中常用的一些总结，难免涉及不全面，还请包涵！**

### 参考内容

https://snailclimb.gitee.io/javaguide/#/

### 欢迎关注

![欢迎关注微信公众号](https://file.chaobei.xyz/blogs/banner_1591192617234.jpg)